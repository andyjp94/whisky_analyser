package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/spf13/viper"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func migrateDB(db *gorm.DB) {
	// Migrate the schema
	var err error
	err = db.AutoMigrate(&Whisky{}).Error
	check(err)
	err = db.AutoMigrate(&Rule{}).Error
	check(err)
	err = db.AutoMigrate(&Lot{}).Error
	check(err)
}

func getDB() (db *gorm.DB) {
	connection := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s sslmode=%s",
		viper.GetString("db_host"),
		viper.GetInt("db_port"),
		viper.GetString("db_user"),
		viper.GetString("db_password"),
		viper.GetString("db_SSL_mode"),
	)
	db, err := gorm.Open(
		"postgres",
		connection,
	)
	check(err)
	return
}

// APIMiddleware will add the db connection to the context
func APIMiddleware(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("databaseConn", db)
		c.Next()
	}
}

func createRoutes(db *gorm.DB) {
	// Creates a gin router with default middleware:
	// logger and recovery (crash-free) middleware
	router := gin.Default()
	router.Use(APIMiddleware(db))
	router.GET("/whisky/:name", getWhisky)
	router.GET("/auction/:id", getAuction)
	router.POST("/auction/:id", createAuction)
	router.GET("/auction/:id/lot/:lotID", getLot)

	// By default it serves on :8080 unless a
	// PORT environment variable was defined.
	router.Run()
	// router.Run(":3000") for a hard coded port
}
func configureConfigReader() {
	viper.SetConfigName("config") // name of config file (without extension)
	viper.SetConfigType("yaml")   // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(".")      // Look for config in the working directory
	viper.AddConfigPath("/etc")   // Look for config when in a Docker image
	err := viper.ReadInConfig()   // Find and read the config file
	if err != nil {               // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %s", err))
	}
	viper.SetEnvPrefix("WHISKY")
	viper.AutomaticEnv()
}

func main() {
	configureConfigReader()
	if viper.GetBool("gin_release_mode") {
		gin.SetMode(gin.ReleaseMode)
	}

	db := getDB()
	defer db.Close()
	migrateDB(db)

	createRoutes(db)

}
