# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.10](https://gitlab.com/andyjp94/whisky_tracker/compare/v0.1.9...v0.1.10) (2020-08-10)

### [0.1.9](https://gitlab.com/andyjp94/whisky_tracker/compare/v0.1.8...v0.1.9) (2020-08-10)


### Features

* Add Dockerfile for use both locally and in prod ([4e5530d](https://gitlab.com/andyjp94/whisky_tracker/commit/4e5530d63e3830db7947fc942e83f7ce1475bb8b))

### [0.1.8](https://gitlab.com/andyjp94/whisky_analyser/compare/v0.1.7...v0.1.8) (2020-08-08)


### Features

* **config:** Change the config to use Viper ([61f4212](https://gitlab.com/andyjp94/whisky_analyser/commit/61f42127091764b750063794dc3c382acff70713))

### [0.1.7](https://gitlab.com/andyjp94/whisky_analyser/compare/v0.1.6...v0.1.7) (2020-08-04)


### Features

* add config support for gin prod setting ([c56e7db](https://gitlab.com/andyjp94/whisky_analyser/commit/c56e7db4dc10f75cd5289ee1b5c1cc474196a320))

### [0.1.6](https://gitlab.com/andyjp94/whisky_analyser/compare/v0.1.5...v0.1.6) (2020-05-24)


### Bug Fixes

* **ci:** only release on non automated commits ([43b1453](https://gitlab.com/andyjp94/whisky_analyser/commit/43b145398cde6ecab0277d7cda62ba46840ec48d))

### [0.1.5](https://gitlab.com/andyjp94/whisky_analyser/compare/v0.1.4...v0.1.5) (2020-05-24)


### Bug Fixes

* **ci:** Update rules to match syntax in docs ([da07a81](https://gitlab.com/andyjp94/whisky_analyser/commit/da07a819bb12f6a7b8a8b46f8a07ca50f22de28e))

### [0.1.4](https://gitlab.com/andyjp94/whisky_analyser/compare/v0.1.3...v0.1.4) (2020-05-24)

### [0.1.3](https://gitlab.com/andyjp94/whisky_analyser/compare/v0.1.2...v0.1.3) (2020-05-24)

### [0.1.2](https://gitlab.com/andyjp94/whisky_analyser/compare/v0.1.1...v0.1.2) (2020-05-24)

### [0.1.1](https://gitlab.com/andyjp94/whisky_analyser/compare/v0.1.0...v0.1.1) (2020-05-24)


### Features

* **ci:** add stage to deploy new versions automatically ([efdba4d](https://gitlab.com/andyjp94/whisky_analyser/commit/efdba4d5bb977814e4cc293b0f93e82de7a02ade))


### Bug Fixes

* **ci:** add keyscan to known hosts ([3b31f7e](https://gitlab.com/andyjp94/whisky_analyser/commit/3b31f7e9bae2389ddb4dfa0b401d65fd742997c8))
* **ci:** change git wording to master since no longer using origin ([460f22f](https://gitlab.com/andyjp94/whisky_analyser/commit/460f22f85624f0e309f76da6c906464dac6f0813))
* **ci:** create directories before using them ([2c45c0b](https://gitlab.com/andyjp94/whisky_analyser/commit/2c45c0b3a43c21deb5af5c363972657559af8f46))
* **ci:** release process errors ([0282e3a](https://gitlab.com/andyjp94/whisky_analyser/commit/0282e3a63506893271a80cd624fbf0e9a1c15e53))
* **ci:** specify local HEAD and remote master in push ([546b3a3](https://gitlab.com/andyjp94/whisky_analyser/commit/546b3a3fee86b2157186a3628953195eca18e964))
* **ci:** syntax error on rule condition ([8b756a2](https://gitlab.com/andyjp94/whisky_analyser/commit/8b756a2f5173fe80fc271e0df852313d5517b8d9))
* **ci:** Update deployment step to push to HEAD ([5549c98](https://gitlab.com/andyjp94/whisky_analyser/commit/5549c98799758276ef1398483749d3345f9ad8bf))

## 0.1.0 (2020-05-10)


### Features

* **api:** Add endpoint for retrieving a lot ([2559f92](https://gitlab.com/andyjp94/whisky_analyser/commit/2559f927e1afe665475b8a35c2d0207b0924b67e))
* **api:** Add endpoint for retrieving an entire auction ([1a04f49](https://gitlab.com/andyjp94/whisky_analyser/commit/1a04f49ef5bc74c30459c58cf4d976f466ae3289))
* **api:** Add endpoint for retrieving an whisky ([9c85efb](https://gitlab.com/andyjp94/whisky_analyser/commit/9c85efb8c9ae55039235c62c0a4a5d862b7cac28))
* Rewrite create auctions to be an API endpoint ([12cc910](https://gitlab.com/andyjp94/whisky_analyser/commit/12cc910048939df30567e20240b4ce9297d4ffd0))
* The auction creation is now working ([7eafd4b](https://gitlab.com/andyjp94/whisky_analyser/commit/7eafd4b3524bd788916fddcfa3dc59eb4c964202))
* **config:** Add options to support reading input files ([e984d69](https://gitlab.com/andyjp94/whisky_analyser/commit/e984d6999da5ce76312940a59a6d51f1e0d6faf0))
* **config:** Fix the loading of the config yml ([41ccb5b](https://gitlab.com/andyjp94/whisky_analyser/commit/41ccb5b0bd842c184a8f2f7735188697fbbb6f62))
* **db:** Add DB models ([051745d](https://gitlab.com/andyjp94/whisky_analyser/commit/051745dbe504daa9d808b3af7e7a22e127016d0d))
* **db:** Add functions for creating lots ([17704e1](https://gitlab.com/andyjp94/whisky_analyser/commit/17704e137e7cbd8cf5dcd0357720380f924fb11f))
* **db:** Call the create function auction ([9def8fe](https://gitlab.com/andyjp94/whisky_analyser/commit/9def8fe66a733f8f1d691cd4d56ec5d4b2d5fed4))
* **db:** Sort out the database structures ([bb65fe2](https://gitlab.com/andyjp94/whisky_analyser/commit/bb65fe2d5746955f4fbe351ada67b5cbb7b7b5c1))
* **error:** Add error handling for db migrations ([bd3f339](https://gitlab.com/andyjp94/whisky_analyser/commit/bd3f33916d2714ded93c38976c7d2156eb3246c6))
* Add support for a yaml config file to control environments ([0497e14](https://gitlab.com/andyjp94/whisky_analyser/commit/0497e14f4911c84870035417d68b460b7fd0092b))
* Add the code to grab all the data and to update it ([5118f9e](https://gitlab.com/andyjp94/whisky_analyser/commit/5118f9eefcbe75b7cc8d7dc32c8cd66fd9f4ede4))


### Bug Fixes

* Add the migrate DB function back into the main function ([5ebcab4](https://gitlab.com/andyjp94/whisky_analyser/commit/5ebcab450167ddb5c4f043ebba8b628f53412b97))
* Mistakes in requirements file ([67599c5](https://gitlab.com/andyjp94/whisky_analyser/commit/67599c5f83c61d668714bf8e30858900b36ecb15))
* regex handling of special chars for checking currency ([5140510](https://gitlab.com/andyjp94/whisky_analyser/commit/51405106128e840ed4c43063134a0d8649d8209b))
* Update go modules ([9991edd](https://gitlab.com/andyjp94/whisky_analyser/commit/9991edd732dc36b898ffd01d5089c8566d0a8f20))
