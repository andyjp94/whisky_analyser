package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/anaskhan96/soup"
)

// func check(e error) {
// 	if e != nil {
// 		panic(e)
// 	}
// }

func parseHTML(filename string) soup.Root {
	wd, err := os.Getwd()
	check(err)
	filePath := fmt.Sprintf("%s/%s", wd, filename)
	fileContents, err := ioutil.ReadFile(filePath)
	check(err)
	return soup.HTMLParse(string(fileContents))
}

func TestGetSaleDetailsWhenExists(t *testing.T) {
	soup := parseHTML("./test/files/single_auction.html")

	currency, price, sold := getSaleDetails(soup)
	if !sold {
		t.Errorf("Item is marked as not sold despite being so.")
	}
	if currency == "" {
		t.Errorf("Currency is not set despite existing")
	}
	if price == 0.00 {
		t.Errorf("Price is not set")
	}
}

func TestGetSaleDetailsWhenNotExists(t *testing.T) {
	soup := parseHTML("./test/files/no_price.html")
	x := soup.FindAll("span", "class", "proddesc")

	currency, price, sold := getSaleDetails(x[0])
	if sold {
		t.Errorf("Item is marked as sold despite not being so.")
	}
	if currency != "" {
		t.Errorf("Currency is set when it shouldn't be, %s", currency)
	}
	if price != 0.00 {
		t.Errorf("Price should be zero, %f", price)
	}
}

func TestGetSaleDetailsWhenDollars(t *testing.T) {
	soup := parseHTML("./test/files/dollar_auction.html")

	currency, price, sold := getSaleDetails(soup)
	if !sold {
		t.Errorf("Item is marked as not sold despite being so.")
	}
	if currency != "USD" {
		t.Errorf("Currency is set to %s, not USD", currency)
	}
	if price == 0.00 {
		t.Errorf("Price is not set")
	}
}

func TestGetLotNumber(t *testing.T) {
	soup := parseHTML("./test/files/single_auction.html")
	x := getLotNumber(soup)
	if x != 2333 {
		t.Errorf("The lot number should have been 2333, should have been %d", x)
	}
}
